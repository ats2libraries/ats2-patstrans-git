#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

#  Copyright (C) 2019 Barry Schwartz
# 
#  This file is part of ATS2 Patstrans.
#  
#  ATS2 Patstrans is free software: you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
#  
#  ATS2 Patstrans is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
#  
#  You should have received a copy of the GNU General Public
#  License along with ATS2 Patstrans.  If not, see
#  <http://www.gnu.org/licenses/>.

#--------------------------------------------------------------------------

  AC_INIT([ATS2 Patstrans],
          [1.0.0],
          [],
          [ats2-patstrans],
          [https://bitbucket.org/ats2libraries/ats2-patstrans/])

  AC_CONFIG_SRCDIR([patstrans.in])

  AC_CONFIG_MACRO_DIR([m4])
  AC_CONFIG_AUX_DIR([build-aux])
  AC_CANONICAL_BUILD            # On what platform are we compiling?
  AC_CANONICAL_HOST             # For what platform are we compiling?
  AC_USE_SYSTEM_EXTENSIONS      # Define such macros as _GNU_SOURCE.
  #AC_CONFIG_TESTDIR([tests])

  AM_INIT_AUTOMAKE([-Wall -Wno-portability])
  AM_SILENT_RULES([yes])
  StM_REQUIRE_GNU_MAKE_IN_PATH
  #m4_ifdef([AM_PROG_AR],[AM_PROG_AR])
  #LT_INIT

  StM_ANALYZE_PACKAGE_VERSION

  m4_define([__propagate_option],[
     m4_define([__shvar],[AS_TR_SH([$1])])
     AC_SUBST(__shvar)
     AC_SUBST([n_]__shvar,[`(test x"${]__shvar[}" = xyes && AS_ECHO([1])) || AS_ECHO([0])`])
     AM_CONDITIONAL(m4_toupper(__shvar), test x"${__shvar}" = xyes)
  ])

#--------------------------------------------------------------------------
#
# Checks for programs.

  #AC_PROG_CC
  #AC_PROG_CC_C99
  #AM_PROG_CC_C_O
  AC_PROG_SED
  AC_PROG_AWK
  AC_PROG_FGREP
  AC_PROG_MKDIR_P
  AC_PROG_LN_S
  AC_PROG_INSTALL
  AC_PROG_MAKE_SET
  #PKG_PROG_PKG_CONFIG

  #
  # Get the *full path* to the first ‘sh’ in PATH.
  #
  # We shall simply presume this is a POSIX-compliant shell.
  # We want to use this result when writing #! shebangs.
  #
  StM_PATH_PROGS_CACHED_AND_PRECIOUS([POSIX_SH],
    [Full path to POSIX shell (sh)],
    [sh],
    [ac_cv_path_POSIX_SH=${ac_path_POSIX_SH}
     ac_path_POSIX_SH_found=:])

  #
  # Get the *full path* to the first ‘awk’ in PATH.
  #
  # We shall simply presume this is a POSIX-compliant Awk.
  # We want to use this result when writing #! shebangs.
  #
  StM_PATH_PROGS_CACHED_AND_PRECIOUS([POSIX_AWK],
    [Full path to POSIX Awk],
    [awk],
    [ac_cv_path_POSIX_AWK=${ac_path_POSIX_AWK}
     ac_path_POSIX_AWK_found=:])

#--------------------------------------------------------------------------
#
# Checks for libraries.

#--------------------------------------------------------------------------
#
# Checks for header files.

#--------------------------------------------------------------------------
#
# Checks for typedefs, structures, and compiler characteristics.

#--------------------------------------------------------------------------
#
# Checks for library functions.

#--------------------------------------------------------------------------

  StM_LIBRARY_MK
  #AC_CONFIG_COMMANDS([tests/library.mk],
  #                   [AS_MKDIR_P([tests]); cp library.mk tests/.])

  StM_CONFIG_MAKEFILES
  #StM_CONFIG_MAKEFILES([tests/Makefile],[tests/GNUmakefile])

  #AC_CONFIG_FILES([tests/atlocal])

  AC_OUTPUT

#--------------------------------------------------------------------------
